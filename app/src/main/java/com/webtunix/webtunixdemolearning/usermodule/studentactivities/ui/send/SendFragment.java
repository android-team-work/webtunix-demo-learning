package com.webtunix.webtunixdemolearning.usermodule.studentactivities.ui.send;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.webtunix.webtunixdemolearning.R;


public class SendFragment extends Fragment {



    private SendViewModel sendViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        sendViewModel =
                ViewModelProviders.of(this).get(SendViewModel.class);
        View root = inflater.inflate(R.layout.fragment_send, container, false);


        TextView textView = root.findViewById(R.id.text_send);



        textView.setText("We design AI solutions\n" +
                "for your Business\n" +
                "Artificial Intelligence Companies fulfil the needs of your enterprise from training data to working with the unorganized text, images and videos for Machine Learning.\n" +
                "AI in your Business reduces the operational costs, increase effectiveness, grows revenue and improves the customer experience. ");

            return root;
    }


}
